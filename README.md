# Ansible role for DHCP Server installation

## Introduction

[ISC DHCP](http://isc.org/products/DHCP/) is a DHCP server implementation,
running as a daemon.

This role installs and configure the server.

## Variables

The following optional variables can be setup as role parameters and act as default if not overriden in the subnet-specific parameters:

- **dns_servers:**: list of DNS servers (expects IPs)
- **domain**: DNS domain name (defaults to the DHCP server DNS domain name)
- **domain_search_list**: DNS search domains (array; defaults to the DHCP server DNS domain name)
- **mtu**: MTU
- **ntp_servers**: list of NTP servers
- **smtp_server**: SMTP server
- **subnets**: list of subnets; hash of parameters defined below

The following variables are parameters for a subnet:

- **subnet**: CIDR of the subnet
- **first_ip**: first IP in this subnet dedicated to the DHCP range (defaults to the first IP of the subnet, excluding the network IP)
- **last_ip**: last IP in this subnet dedicated to the DHCP range (defaults to the second to last IP of the subnet, excluding the broadcast IP, the last IP being supposedly reserved for the router)
- **pxe**: activates PXE support, the following parameters are necessary:
    - **tftp_server**: TFTP server
    - **filename**: filename to fetch via TFTP
- **routers**: list of routers (gateways) (expects IPs)

